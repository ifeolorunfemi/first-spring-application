package com.example.my_first_app.student;

import java.util.*;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

    private final StudentRepository sr;

    @Autowired
    public StudentService(StudentRepository sr) {
        this.sr = sr;
    }

    public List<Student> getStudents() {
        return sr.findAll();
    }

    public void addStudents(Student student) {

        Optional<Student> optionalStudent = sr.findStudentByEmail(student.getEmail());

        if (optionalStudent.isPresent()) {
            throw new IllegalAccessError("Email already exists");
        }

        sr.save(student);

    }

    public void deleteStudent(Long id) {

        if (!sr.existsById(id)) {
            throw new IllegalAccessError(" Student ID does not exist");

        }
        sr.deleteById(id);
    }

    public Optional<Student> getStudentById(Long id) {
        if (!sr.existsById(id)) {
            throw new IllegalAccessError(" Student ID does not exist");

        }
        return sr.findById(id);
    }

    @Transactional
    public void updateStudent(Long id, String name, String email) {
        Optional<Student> optionalStudent = sr.findById(id);
        if (!optionalStudent.isPresent()) {

            throw new IllegalAccessError("User by id doesnt exist");
        }
        if (name != null) {

            optionalStudent.get().setName(name);
        }
        if (sr.findStudentByEmail(email).isPresent()) {

            throw new IllegalAccessError("Email already exists");
        }
        if (email != null
                && (!Objects.equals(email, optionalStudent.get().getEmail()))) {

            optionalStudent.get().setEmail(email);
        }

    }
}
