package com.example.my_first_app.student;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository studentRepository) {
        return args -> {

            Student emmanue = new Student(
                    "emmanuel",
                    "ifeolorunfemi@gmail.com",
                    LocalDate.of(1997, Month.DECEMBER, 18)

            );
            Student ife = new Student(
                    "ife",
                    "ifeolorunfemi@gmail.com",
                    LocalDate.of(2018, Month.DECEMBER, 18));

            studentRepository.saveAll(
                    List.of(emmanue, ife));
        };
    }
}
